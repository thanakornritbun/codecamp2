'use strict'

const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')
const mysql = require('mysql2/promise')

const pool  = mysql.createPool({
    connectionLimit : 10,
    host            : 'localhost',
    user            : 'root',
    password        : '',
    database        : 'db2'
 });
 

const app = new Koa()
const router = new Router()


render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})


router.get('/', ctx => {
    ctx.body = 'this is index page'
})

async function myQuery() {
    let [rows, fields] = await pool.query('SELECT id,firstname,lastname,salary,role FROM user');
    console.log('The solution is: ', rows, fields);
 }
 myQuery();
 

app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())


app.listen(3005)

