fetch('homework2_1.json').then(response => {
    return response.json();
})
    .then(peopleSalary => {
        console.log(peopleSalary);
        let peopleLowSalary = peopleSalary
            .filter(chkLowSalary)
            .map(multiSalary)
        console.log(peopleLowSalary);
        //console.log(peopleSalary);
        let sumSalary = peopleSalary.reduce((sum, num) => sum + num.salary, 0)
        console.log(sumSalary);
        gentable(peopleSalary);
    })
    .catch(error => {
        console.error('Error:', error);
    });

function chkLowSalary(num) {
    return num.salary < 100000;
}
function multiSalary(num) {
    num.salary = num.salary * 2;
    return num;
}
function gentable(peopleSalary) {
    let htmlhead = '<thead><tr>';
    for (let head in peopleSalary[0]) {
        htmlhead += '<th>' + head + '</th>';
    }
    htmlhead += '</tr></thead>'
    $("#myTable").append($(htmlhead));
    let htmlStr = '<tr>';
    for (let i = 0; i < peopleSalary.length; i++) {
        htmlStr += '</tr>'
        for (let data in peopleSalary[i]) {
            htmlStr += '<td>' + peopleSalary[i][data] + '</td>';
        }
    }
    $("#myTable").append($(htmlStr));
}