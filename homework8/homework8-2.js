'use strict'

let fs = require('fs');
let results = '';
/*
fs.readFile('head.txt', 'utf8',function  (err, data) {
    results=results+data+'\n';
    console.log(results);
    fs.readFile('body.txt', 'utf8', function (err, data) {
        results=results+data+'\n';
        console.log(results);
        fs.readFile('leg.txt', 'utf8', function (err, data) {
            results=results+data+'\n';
            console.log(results);
            fs.readFile('feet.txt', 'utf8', function (err, data) {
                results=results+data+'\n';
                console.log(results);
                fs.writeFile('robot.txt', results, 'utf8', (err) => {
                    console.log('write robot complete!!');
                  });                  
            });
        });
    });
});
*/
function readFileHead() {
    fs.readFile('head.txt', 'utf8', readFileBody)
}
function readFileBody(err, data) {
    if (err) {
        console.error(err);
        return;
    }
    results = results + data + '\n';
    fs.readFile('body.txt', 'utf8', readFileLeg)
}
function readFileLeg(err, data) {
    if (err) {
        console.error(err);
        return;
    }
    results = results + data + '\n';
    fs.readFile('leg.txt', 'utf8', readFileFeet)
}
function readFileFeet(err, data) {
    if (err) {
        console.error(err);
        return;
    }
    results = results + data + '\n';
    fs.readFile('feet.txt', 'utf8', readFileComplete)
}
function readFileComplete(err, data) {
    if (err) {
        console.error(err);
        return;
    }
    results = results + data + '\n';
    fs.writeFile('robot.txt',results,'utf8', writeFileCompleted)
}   
function writeFileCompleted(err) {
    if (err) {
        console.error(err);
        return;
    }      
}
readFileHead();