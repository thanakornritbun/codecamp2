'use strict'

let fs = require('fs')
let {promisify} = require('util')

let readFile = promisify(fs.readFile);
let writeFile = promisify(fs.writeFile) 



async function copyFile() {
    try {
        let result='';
        let data ='';
        data = await readFile('head.txt','utf8',)
        result += data +'\n'
        data = await readFile('body.txt','utf8')
        result += data +'\n'
        data = await readFile('leg.txt','utf8')
        result += data +'\n'
        data = await readFile('feet.txt','utf8')
        result += data +'\n'
        console.log(result);
        writeFile('robot.txt',result,'utf8')
    } catch (error) {
      console.error(error);
    }
  }
  copyFile();
  