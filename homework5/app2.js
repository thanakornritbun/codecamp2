
/*
$(function () {
    $('#btnOK').click(function () {
        $.get('https://swapi.co/api/people/').then(function (res) {
            console.log(res);
            // $('#result').append("name:" + res.name);
            let tableHeader = Object.keys(res[0])

            $('#myTable').append($('<tr id="tableHeader"></tr>'));
            for (let header in tableHeader) {
                $('#tableHeader').append($('<th>' + tableHeader[header] + '</th>'));
            }
            for (let people in res) {
                let peopleObject = res[people]
                $('#myTable').append($('<tr>'));
                for (let key in peopleObject) {
                    $('#myTable').append($('<td>' + peopleObject[key] + '</td>'));
                }
                $('#myTable').append($('</tr>'));
            }
        });
    });
});
*/


$(function (){
    $.get('https://swapi.co/api/people/').then(function (res){
        console.log(res.results);
        // $('#result').append("name:" + res.name);
        let tableHeader = Object.keys(res.results[0])

        $('#myTable').append($('<tr id="tableHeader"></tr>'));
        for (let header in tableHeader) {
            $('#tableHeader').append($('<th>' + tableHeader[header] + '</th>'));
        }
        for (let people in res.results) {
            let peopleObject = res.results[people]
            $('#myTable').append($('<tr>'));
            for (let key in peopleObject) {
                $('#myTable').append($('<td>' + peopleObject[key] + '</td>'));
            }
            $('#myTable').append($('</tr>'));
        }
    })
});