const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')

const app = new Koa()
const router = new Router()

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

router.get('/images', async ctx => {
    await ctx.render('template')
})




router.get('/', ctx => {
    ctx.body = 'hello world'
})

router.get('/about', ctx => {
    ctx.body = 'This is about'
})

app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())


app.listen(3004)